<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/28/2017
 * Time: 11:12 AM
 */

namespace App;

use PDO,PDOException;
class Database
{
    public $dbh;

    public function __construct()
    {

        try {
            $this->dbh = new PDO('mysql:host=localhost;dbname=greading_system_b44', "root", "");
            $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            echo "database connection successful ! <br>";
        }

        catch (PDOException $error) {
          echo  $error->getMessage() . "<br/>";
            die();
        }
    }
}