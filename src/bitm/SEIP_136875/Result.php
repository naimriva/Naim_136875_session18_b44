<?php


namespace App;


class Result extends Database
{   private $id;
    private $name;
    private $roll;
    private $department;
    private $markBangla;
    private $markEnglish;
    private $markMath;

    private $gradeBangla;
    private $gradeEnglish;
    private $gradeMath;

    public function setData($postData)//data gula k post er madhomme set kora
    {
        if(array_key_exists("id",$postData))//id namok key ta jodi oi array er modde thake tahole oi id k property er moddhe rekhe dibo
        {
            $this->id=$postData['id'];
        }

        if(array_key_exists("name",$postData)){
            $this->name=$postData['name'];
        }

        if(array_key_exists("roll",$postData))
        {
            $this->roll=$postData['roll'];
        }

        if(array_key_exists("department",$postData))
        {
            $this->department=$postData['department'];
        }

        if(array_key_exists("markBangla",$postData))
        {
            $this->markBangla=$postData['markBangla'];
            $this->gradeBangla=$this->convertMark2Grade($this->gradeBangla);
        }

        if(array_key_exists("markEnglish",$postData))
        {
            $this->markEnglish=$postData['markEnglish'];
            $this->gradeEnglish=$this->convertMark2Grade($this->gradeEnglish);
        }

        if(array_key_exists("markMath",$postData))
        {
            $this->markMath=$postData['$markMath'];
            $this->gradeMath=$this->convertMark2Grade($this->gradeMath);
        }





    }


    public function convertMark2Grade($mark)
    {
       switch($mark)
       {
           case $mark>79: return "A+";

           case $mark>74: return "A";

           case $mark>69: return "A-";

           case $mark>59: return "B";

           case $mark>54: return "B-";

           case $mark>49: return "C";

           case $mark>44: return "C+";

           case $mark>39: return "D";

           default: return "F";
 }
       }

    public function store(){

    $arrayData=array($this->name,$this->roll,$this->department,$this->markBangla,$this->markEnglish,$this->markMath,$this->gradeBangla,$this->gradeEnglish,$this->gradeMath);

        $sql="Insert into result(name,roll,department,mark_bangla,mark_english,mark_math,grade_bangla,grade_english,grade_math) VALUES (?,?,?,?,?,?,?,?,?)";

     $sth=$this->dbh->prepare($sql);
        $success=$sth->execute($arrayData);

        if($success)
        {
            message::message("Success! data has been insrted sucssfully! <br>");
        }
        else
        {
            message::message("failed! data has not been insrted sucssfully! <br>");
        }

        Utility::redirect("InformationCollection.php");
    }
}



























