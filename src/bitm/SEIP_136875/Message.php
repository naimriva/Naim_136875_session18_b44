<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/28/2017
 * Time: 11:26 AM
 */

namespace App;


class Message
{
    public static function message($msg=null)
    {
        if(is_null($msg))
        {
           return self::getmessage();
        }
        else
        {
            self::setmessage();
        }
    }


    public static function setmessage($msg=null)
    {
        $_SESSION['message']=$msg;
    }

    public static function getmessage($msg=null)
    {
        $_SESSION['message']=$msg;
    }


}